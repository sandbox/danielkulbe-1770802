# Tweet!

Put twitter on your website with Tweet!, an unobtrusive javascript plugin for jquery.

## History, demos & examples

Tweet javascript was originally maintained by [seaofclouds](http://github.com/seaofclouds/) but I needed some customizions for my Drupal module. So I created my own [repository](https://github.com/Umbrielsama/tweet) and included this into this module.

For demos and examples see [tweet.seaofclouds.com](http://tweet.seaofclouds.com) (original version), or the bundled <code>index.html</code> file.

## Features

* small size and fast download time
* will not slow down or pause your page while tweets are loading
* display up to 100 tweets, as permitted by the twitter search api
* display tweets from a twitter search, or from your own feed
* optional verb tense matching, for human readable tweets
* optionally display your avatar
* optionally display tweets from multiple accounts!
* optionally display format an absolute time
* automatic linking of @replies to users' twitter page
* automatic linking of URLs
* automatic linking of #hashtags, to a twitter search of all your tags
* converts <3 to a css styleable ♥ (we ♥ hearts)
* customize the style with your own stylesheet or with other jquery plugins
* customize the layout with a user-defined template function
* supports RequireJS and other AMD-compatible javascript loaders

## Usage

So simple: Just enable the block, put it in a region you wish and configure it at the block's configuration page.

## Theming

Also simple: Copy the included tweet.css to your theme directory an style it like you want!

##Performance

The module uses Drupal's global cache, it has a very small size and a fast download time.



Hope it will also be useful for you!